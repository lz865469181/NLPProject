import re
from urllib import parse

import scrapy

from myspider.items import TieBaItem


class QizhaSpider(scrapy.Spider):
    name = 'qizha'
    allowed_domains = ['tieba.baidu.com']
    start_urls = ['https://tieba.baidu.com/f?ie=utf-8&kw=%E4%BF%A1%E7%94%A8']

    def parse(self, response):
        #页面中帖子的url地址
        url_list = response.css(".j_th_tit::attr(href)").extract()
        for url in url_list:
            if url == "bawu2/errorPage":
                continue
            print(url)
            yield scrapy.Request(url=parse.urljoin(response.url, url), callback=self.parse_detail) #生成器的方式，将参数传给其他函数处理

        next_url = response.css(".next.pagination-item::attr(href)").extract()[0]
        if next_url:
            yield scrapy.Request(url=parse.urljoin(response.url, next_url), callback=self.parse)

    def parse_detail(self, response):
        #获取页面中的标题
        title = response.css(".core_title_txt.pull-left.text-overflow::text").extract()
        if title:
            authors = response.css(".p_author_name.j_user_card::text").extract()
            contents = response.css(".d_post_content.j_d_post_content").extract()
            # 进一步处理帖子的内容 包含图片地址以及前端的换行标签
            content_list = self.get_content(contents)
            # 处理帖子的发送时间和帖子位于楼数
            bbs_sendtime_list, bbs_floor_list = self.get_send_time_and_floor(response)

            for i in range(len(authors)):
                tieba_item = TieBaItem()
                tieba_item["title"] = title[0]
                tieba_item["author"] = authors[i]
                tieba_item["content"] = content_list[i]
                tieba_item["reply_time"] = bbs_sendtime_list[i]
                tieba_item["floor"] = bbs_floor_list[i]
                return  tieba_item
            #print(title, authors, contents, bbs_sendtime_list)

    def get_content(self, contents):
        content_list = []
        for content in contents:
            reg = ";\">(.*)</div>"
            result = re.findall(reg, content)[0]
            content_list.append(result)

        return  content_list

    def get_send_time_and_floor(self, response):
        bbs_sendtime_and_floor_list = response.css(".post-tail-wrap span[class=tail-info]::text").extract()
        i = 0 # 0是楼数  1是时间
        bbs_sendtime_list = []
        bbs_floor_list = []

        for bbs_sendtime_and_floor in bbs_sendtime_and_floor_list:
            if i % 3 == 1:
                bbs_sendtime_list.append(bbs_sendtime_and_floor)
            elif i % 3 == 2:
                bbs_floor_list.append(bbs_sendtime_and_floor)
            i += 1
        return bbs_sendtime_list, bbs_floor_list