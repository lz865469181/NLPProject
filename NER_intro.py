import nltk
print("NTLK version: %s" % (nltk.__version__))

# nltk.download('words')
# nltk.download('averaged_perceptron_tagger')
# nltk.download('punkt')
# nltk.download("maxent_ne_chunker")

paragraph1 = "Artificial Intelligence (AI) is the talk of the world and it features prominently in predictions \
for 2019 (see here and here) and recent surveys by consulting firms and other observers of the tech scene. \
Here are the key findings: Consumer adoption: “Smart speakers” lead the way to the AI-infused home of the future\
Smart speakers (e.g., Amazon Echo and Google Home) will become the fastest-growing connected device category \
in history, with an installed base projected to surpass 250 million units by the end of 2019. With sales of \
164 million units at an average selling price of $43 per unit, total smart speakers’ revenues will reach $7 billion, \
up 63% from 2018. "
from nltk import word_tokenize, pos_tag, ne_chunk
paragraph2 = "Beijing, Shanghai, Tianjin"

results = ne_chunk(pos_tag(  (paragraph1)))

print('The sentence is : %s' % (paragraph1))
for x in str(results).split("\n"):
    if '/NNP' in x:
        print(x)


#Method 1 Majority_Voting 获取最多数量的特性
import pandas as pd
import numpy as np
data = pd.read_csv("./data/ner_dataset.csv", encoding="latin1")
data = data.fillna(method="ffill")
print(data.tail(10))

words = list(set(data["Word"].values))
n_words = len(words)
print(n_words)

from sklearn.base import BaseEstimator, TransformerMixin
class MajorityVotingTagger(BaseEstimator, TransformerMixin):
    def fit(self, X, y):
        """
        X:list of words
        y:list of tags
        """
        word2cnt = {}
        self.tags = []
        for x, t in zip(X, y):
            #判断当前tag 是否存在，不存在添加到标签中
            if t not in self.tags:
                self.tags.append(t)
            #如果存在,进行+1
            if x in word2cnt:
                if t in word2cnt[x]:
                    word2cnt[x][t] += 1
                else:
                    word2cnt[x][t] = 1
            else:
                word2cnt[x] = {t:1}

        self.mjvote = {}
        #取参数值得最大
        for k,d in word2cnt.items():
            self.mjvote[k] = max(d, key = d.get)

    def predict(self, X, y = None):
        """Predict the tag from memory.If word is unknown, predict '0'"""
        return [self.mjvote.get(x, '0') for x in X]

words = data['Word'].values.tolist()
tags = data["Tag"].values.tolist()

from sklearn.model_selection import cross_val_predict
from sklearn.metrics import classification_report
pred = cross_val_predict(estimator=MajorityVotingTagger(), X = words, y = tags, cv = 5)
report = classification_report(y_pred=pred, y_true=tags)
print(report)

from sklearn.ensemble import RandomForestClassifier
def get_feature(word):
    return np.array([word.istitle(), word.islower(), word.isupper(), len(word), word.isdigit(), word.isalpha()])

words = [get_feature(w) for w in data["Word"].values.tolist()]
tags = data["Tag"].values.tolist()

pred = cross_val_predict(RandomForestClassifier(n_estimators=20), X = words, y = tags, cv = 5)
report = classification_report(y_pred=pred, y_true=tags)
print(report)

def get_sentences(data):
    agg_func = lambda s : [(w, p, t) for w, p, t in zip(s["Word"].values.to_list(), s["POS"].values.tolist(), s["Tag"].values.tolist())]
    sentence_grouped = data.groupby("Sentence #").apply(agg_func)
    return [s for s in sentence_grouped]

from sklearn.preprocessing import LabelEncoder
out = []
y = []
mv_tagger = MajorityVotingTagger()
tag_encoder = LabelEncoder()
pos_encoder = LabelEncoder()

words = data["Word"].values.tolist()
pos = data["POS"].values.tolist()
tags = data["Tag"].values.tolist()

mv_tagger.fit(words, tags)
tag_encoder.fit(tags)
pos_encoder.fit(pos)

sentences = get_sentences(data)

for sentence in sentences:
    for i in range(len(sentence)):
        w, p, t = sentence[i][0], sentence[i][1], sentence[i][2]

        if i < len(sentence)-1:
            #如果不是最后一个单词，则可以用到下文的信息
            mem_tag_r = tag_encoder.transform(mv_tagger.predict())
            true_pos_r = pos_encoder.transform([sentence[i+1][1]])[0]
        else:
            mem_tag_r = tag_encoder.transform(['O'])[0]
            true_pos_r = pos_encoder.transform(['.'])[0]

        if i > 0:
            #如果不是第一个单词，则可以用到上文的信息
            mem_tag_l = tag_encoder.transform(mv_tagger.predict([sentence[i-1][0]]))[0]
            true_pos_l = pos_encoder.transform([sentence[i-1][1]])[0]
        else:
            mem_tag_l = tag_encoder.transform(['O'])[0]
            true_pos_l = pos_encoder.transform(['.'])[0]
        # print (mem_tag_r, true_pos_r, mem_tag_l, true_pos_l)

        out.append(np.array([w.istitle(), w.islower(), w.isupper(), len(w), w.isdigit(), w.isalpha(),
                             tag_encoder.transform(mv_tagger.predict([w])),
                             pos_encoder.transform([p])[0], mem_tag_r, true_pos_r, mem_tag_l, true_pos_l]))
        y.append(t)

from sklearn.model_selection import cross_val_predict
from sklearn.metrics import classification_report
pred = cross_val_predict(RandomForestClassifier(n_estimators=20), X=out, y=y, cv=5)
report = classification_report(y_pred=pred, y_true=y)
print(report)


#CRF 逻辑
def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]

    features = {
        'bias': 1.0,
        'word.lower()':word.lower(),
        'word[-3:]':word[-3:],
        'word[-2:]': word[-2:],
        'word.isupper()': word.isupper(),
        'word.istitle()': word.istitle(),
        'word.isdigit()': word.isdigit(),
        'postag' : postag,
        'postag[:2]' : postag[:2],
    }

    if i > 0: #word_prev, word_curr
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.update({
            '-1:word.lower()': word1.lower(),
            '-1:word.istitle()': word1.istitle(),
            '-1:word.isupper()': word1.isupper(),
            '-1:postag': postag1,
            '-1:postag[:2]': postag1[:2],
        })
    else:
        features['BOS'] = True

    if i < len(sent) - 1:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.update({
            '+1:word.lower()': word1.lower(),
            '+1:word.istitle()': word1.istitle(),
            '+1:word.isupper()': word1.isupper(),
            '+1:postag': postag1,
            '+1:postag[:2]': postag1[:2],
        })
    else:
        features['EOS'] = True

    return features

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]

from sklearn_crfsuite import CRF

crf = CRF(algorithm='lbfgs',
          c1=0.1,
          c2=0.1,
          max_iterations=100)
from sklearn_crfsuite.metrics import flat_classification_report
report = flat_classification_report(y_pred=pred, y_true=y)
print(report)
