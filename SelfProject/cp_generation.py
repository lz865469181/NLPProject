import tensorflow as tf
import os
import numpy as np

#加载数据集
def load_data(path):
    input_file = os.path.join(path)
    with open(input_file, "r") as f:
        data = f.read()

    return data

text = load_data("./data/cp.txt")
print(text)

def create_lookup_tables():
    vocab_to_int = {str(ii).zfill(3) : ii for ii in range(1000)}
    int_to_vocab = {ii: str(ii).zfill(3) for ii in range(1000)}
    return vocab_to_int, int_to_vocab

import pickle

words = [word for word in text.split()]

reverse_words = [text.split()[idx] for idx in range(len(words)-1, 0, -1)]
vocab_to_int, int_to_vocab = create_lookup_tables()

int_text = [vocab_to_int[word] for word in reverse_words]
#映射关系存储到本地
pickle.dump((int_text, vocab_to_int, int_to_vocab), open('process.p', 'wb'))

# # 读取保存的数据
# int_text, vocab_to_int, int_to_vocab = pickle.load(open('preprocess.p', mode='rb'))

def get_batches(int_text, batch_size, seq_length):
    batchCnt = len(int_text) #(batch_size * seq_length)

    int_text_inputs = int_text[:batchCnt * (batch_size * seq_length)]
    int_text_targets = int_text[1:batchCnt * (batch_size * seq_length) + 1]

    result_list = []
    x = np.array(int_text_inputs).reshape(1, batch_size, -1)
    y = np.array(int_text_targets).reshape(1, batch_size, -1)

    x_new = np.dsplit(x, batchCnt)
    y_new = np.dsplit(y, batchCnt)

    for ii in range(batchCnt):
        x_list = []
        x_list.append(x_new[ii][0])
        x_list.append(y_new[ii][0])
        result_list.append(x_list)

    return np.array(result_list)

 # 训练迭代次数
epochs = 50
# 批次大小
batch_size = 32
# RNN的大小（隐藏节点的维度）
rnn_size = 512
# 嵌入层的维度
embed_dim = 512
# 序列的长度，始终为1
seq_length = 1
# 学习率
learning_rate = 0.01
# 过多少batch以后打印训练信息
show_every_n_batches = 10

save_dir = './save'

#构建计算图
#使用实现的神经网络构建计算图。使用normalized_embedding做相似度上距离的计算。

# 利用这个可清空default graph以及nodes
tf.reset_default_graph()

train_graph = tf.Graph()
with train_graph.as_default():
    vocab_size = len(int_to_vocab)
    # 定义输入、目标和学习率占位符
    input_text = tf.placeholder(tf.int32, [None, None], name="input")
    targets = tf.placeholder(tf.int32, [None, None], name="targets")
    lr = tf.placeholder(tf.float32)

    input_data_shape = tf.shape(input_text)
    # 构建RNN单元并初始化
    # 将一个或多个BasicLSTMCells 叠加在MultiRNNCell中，这里我们使用2层LSTM cell
    cell = tf.contrib.rnn.MultiRNNCell([tf.contrib.rnn.BasicLSTMCell(num_units=rnn_size) for _ in range(2)])
    initial_state = cell.zero_state(input_data_shape[0], tf.float32)
    initial_state = tf.identity(initial_state, name="initial_state")

    # embed_matrix是嵌入矩阵，后面计算相似度(距离)的时候会用到
    embed_matrix = tf.Variable(tf.random_uniform([vocab_size, embed_dim], -1, 1))
    # embed_layer是从嵌入矩阵（查找表）中索引到的向量
    embed_layer = tf.nn.embedding_lookup(embed_matrix, input_text)

    # 使用RNN单元构建RNN
    outputs, state = tf.nn.dynamic_rnn(cell, embed_layer, dtype=tf.float32)
    final_state = tf.identity(state, name="final_state")

    logits = tf.layers.dense(outputs, vocab_size)

    probs = tf.nn.softmax(logits, name='probs')

    cost = tf.contrib.seq2seq.sequence_loss(
        logits,
        targets,
        tf.ones([input_data_shape[0], input_data_shape[1]]))

    norm = tf.sqrt(tf.reduce_sum(tf.square(embed_matrix), 1, keep_dims=True))
    normalized_embedding = embed_matrix / norm

    optimizer = tf.train.AdamOptimizer(lr)

    gradients = optimizer.compute_gradients(cost)
    capped_gradients = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gradients if
                        grad is not None]  # clip_by_norm
    train_op = optimizer.apply_gradients(capped_gradients)

    correct_pred = tf.equal(tf.argmax(probs, 2),
                            tf.cast(targets, tf.int64))  # logits <--> probs  tf.argmax(targets, 1) <--> targets
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32), name='accuracy')

import seaborn as sns
import matplotlib.pyplot as plt

batches = get_batches(int_text[:-(batch_size + 1)], batch_size, seq_length)
test_batches = get_batches(int_text[-(batch_size + 1):], batch_size, seq_length)
top_k = 10
topk_acc_list = []
topk_acc = 0
sim_topk_acc_list = []
sim_topk_acc = 0

range_k = 5
floating_median_idx = 0
floating_median_acc_range_k = 0
floating_median_acc_range_k_list = []

floating_median_sim_idx = 0
floating_median_sim_acc_range_k = 0
floating_median_sim_acc_range_k_list = []

losses = {'train': [], 'test': []}
accuracies = {'accuracy': [], 'topk': [], 'sim_topk': [], 'floating_median_acc_range_k': [],
              'floating_median_sim_acc_range_k': []}

with tf.Session(graph=train_graph) as sess:
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    for epoch_i in range(epochs):
        state = sess.run(initial_state, {input_text: batches[0][0]})

        # 训练的迭代，保存训练损失
        for batch_i, (x, y) in enumerate(batches):
            feed = {
                input_text: x,
                targets: y,
                initial_state: state,
                lr: learning_rate}
            train_loss, state, _ = sess.run([cost, final_state, train_op], feed)  #
            losses['train'].append(train_loss)

            # Show every <show_every_n_batches> batches
            if (epoch_i * len(batches) + batch_i) % show_every_n_batches == 0:
                print('Epoch {:>3} Batch {:>4}/{}   train_loss = {:.3f}'.format(
                    epoch_i,
                    batch_i,
                    len(batches),
                    train_loss))

        # 使用测试数据的迭代
        acc_list = []
        prev_state = sess.run(initial_state, {input_text: np.array([[1]])})  # test_batches[0][0]
        for batch_i, (x, y) in enumerate(test_batches):
            # Get Prediction
            test_loss, acc, probabilities, prev_state = sess.run(
                [cost, accuracy, probs, final_state],
                {input_text: x,
                 targets: y,
                 initial_state: prev_state})  #

            # 保存测试损失和准确率
            acc_list.append(acc)
            losses['test'].append(test_loss)
            accuracies['accuracy'].append(acc)

            print('Epoch {:>3} Batch {:>4}/{}   test_loss = {:.3f}'.format(
                epoch_i,
                batch_i,
                len(test_batches),
                test_loss))

            # 利用嵌入矩阵和生成的预测计算得到相似度矩阵sim
            valid_embedding = tf.nn.embedding_lookup(normalized_embedding, np.squeeze(probabilities.argmax(2)))
            similarity = tf.matmul(valid_embedding, tf.transpose(normalized_embedding))
            sim = similarity.eval()

            # 保存预测结果的Top K准确率和与预测结果距离最近的Top K准确率
            topk_acc = 0
            sim_topk_acc = 0
            for ii in range(len(probabilities)):

                nearest = (-sim[ii, :]).argsort()[0:top_k]
                if y[ii] in nearest:
                    sim_topk_acc += 1

                if y[ii] in (-probabilities[ii]).argsort()[0][0:top_k]:
                    topk_acc += 1

            topk_acc = topk_acc / len(y)
            topk_acc_list.append(topk_acc)
            accuracies['topk'].append(topk_acc)

            sim_topk_acc = sim_topk_acc / len(y)
            sim_topk_acc_list.append(sim_topk_acc)
            accuracies['sim_topk'].append(sim_topk_acc)

            # 计算真实值在预测值中的距离数据
            realInSim_distance_list = []
            realInPredict_distance_list = []
            for ii in range(len(probabilities)):
                sim_nearest = (-sim[ii, :]).argsort()
                idx = list(sim_nearest).index(y[ii])
                realInSim_distance_list.append(idx)

                nearest = (-probabilities[ii]).argsort()[0]
                idx = list(nearest).index(y[ii])
                realInPredict_distance_list.append(idx)

            print('真实值在预测值中的距离数据：')
            print('max distance : {}'.format(max(realInPredict_distance_list)))
            print('min distance : {}'.format(min(realInPredict_distance_list)))
            print('平均距离 : {}'.format(np.mean(realInPredict_distance_list)))
            print('距离中位数 : {}'.format(np.median(realInPredict_distance_list)))
            print('距离标准差 : {}'.format(np.std(realInPredict_distance_list)))

            print('真实值在预测值相似向量中的距离数据：')
            print('max distance : {}'.format(max(realInSim_distance_list)))
            print('min distance : {}'.format(min(realInSim_distance_list)))
            print('平均距离 : {}'.format(np.mean(realInSim_distance_list)))
            print('距离中位数 : {}'.format(np.median(realInSim_distance_list)))
            print('距离标准差 : {}'.format(np.std(realInSim_distance_list)))
            #             sns.distplot(realInPredict_distance_list, rug=True)  #, hist=False
            # plt.hist(np.log(realInPredict_distance_list), bins=50, color='steelblue', normed=True )

            # 计算以距离中位数为中心，范围K为半径的准确率
            floating_median_sim_idx = int(np.median(realInSim_distance_list))
            floating_median_sim_acc_range_k = 0

            floating_median_idx = int(np.median(realInPredict_distance_list))
            floating_median_acc_range_k = 0
            for ii in range(len(probabilities)):
                nearest_floating_median = (-probabilities[ii]).argsort()[0][
                                          floating_median_idx - range_k:floating_median_idx + range_k]
                if y[ii] in nearest_floating_median:
                    floating_median_acc_range_k += 1

                nearest_floating_median_sim = (-sim[ii, :]).argsort()[
                                              floating_median_sim_idx - range_k:floating_median_sim_idx + range_k]
                if y[ii] in nearest_floating_median_sim:
                    floating_median_sim_acc_range_k += 1

            floating_median_acc_range_k = floating_median_acc_range_k / len(y)
            floating_median_acc_range_k_list.append(floating_median_acc_range_k)
            accuracies['floating_median_acc_range_k'].append(floating_median_acc_range_k)

            floating_median_sim_acc_range_k = floating_median_sim_acc_range_k / len(y)
            floating_median_sim_acc_range_k_list.append(floating_median_sim_acc_range_k)
            accuracies['floating_median_sim_acc_range_k'].append(floating_median_sim_acc_range_k)

        print('Epoch {:>3} floating median sim range k accuracy {} '.format(epoch_i, np.mean(
            floating_median_sim_acc_range_k_list)))  #:.3f
        print('Epoch {:>3} floating median range k accuracy {} '.format(epoch_i, np.mean(
            floating_median_acc_range_k_list)))  #:.3f
        print('Epoch {:>3} similar top k accuracy {} '.format(epoch_i, np.mean(sim_topk_acc_list)))  #:.3f
        print('Epoch {:>3} top k accuracy {} '.format(epoch_i, np.mean(topk_acc_list)))  #:.3f
        print('Epoch {:>3} accuracy {} '.format(epoch_i, np.mean(acc_list)))  #:.3f

    # Save Model
    saver.save(sess, save_dir)  # , global_step=epoch_i
    print('Model Trained and Saved')
    embed_mat = sess.run(normalized_embedding)

sns.distplot(realInSim_distance_list, rug=True)

sns.distplot(realInPredict_distance_list, rug=True)

plt.plot(losses['train'], label='Training loss')
plt.legend()
_ = plt.ylim()

plt.plot(losses['test'], label='Test loss')
plt.legend()
_ = plt.ylim()

#测试准确率Top K准确率相似度Top K准确率浮动距离中位数Range K准确率
plt.plot(accuracies['accuracy'], label='Accuracy')
plt.plot(accuracies['topk'], label='Top K')
plt.plot(accuracies['sim_topk'], label='Similar Top K')
plt.plot(accuracies['floating_median_acc_range_k'], label='Floating Median Range K Acc')
plt.plot(accuracies['floating_median_sim_acc_range_k'], label='Floating Median Sim Range K Acc')
plt.legend()
_ = plt.ylim()


for batch_i, (x, y) in enumerate(test_batches):
    plt.plot(y, label='Targets')
    plt.plot(np.squeeze(probabilities.argmax(2)), label='Prediction')
    plt.legend()
    _ = plt.ylim()

pickle.dump((seq_length, save_dir), open('params.p', 'wb'))

_, vocab_to_int, int_to_vocab = pickle.load(open('preprocess.p', mode='rb'))

seq_length, load_dir = pickle.load(open('params.p', mode='rb'))

def get_tensors(loaded_graph):
    inputs = loaded_graph.get_tensor_by_name("input:0")
    initial_state = loaded_graph.get_tensor_by_name("initial_state:0")
    final_state = loaded_graph.get_tensor_by_name("final_state:0")
    probs = loaded_graph.get_tensor_by_name("probs:0")
    return inputs, initial_state, final_state, probs

def pick_word(probabilities, sim, int_to_vocab, top_n = 5, pred_mode = 'sim'):

    vocab_size = len(int_to_vocab)

    if pred_mode == 'sim':
        p = np.squeeze(sim)
        p[np.argsort(p)[:-top_n]] = 0
        p = p / np.sum(p)
        c = np.random.choice(vocab_size, 1, p=p)[0]
        return int_to_vocab[c]
    elif pred_mode == 'median':
        p = np.squeeze(sim)
        p[np.argsort(p)[:floating_median_sim_idx - top_n]] = 0
        p[np.argsort(p)[floating_median_sim_idx + top_n:]] = 0
        p = np.abs(p) / np.sum(np.abs(p))
        c = np.random.choice(vocab_size, 1, p=p)[0]
        return int_to_vocab[c]
    elif pred_mode == 'topk':
        p = np.squeeze(probabilities)
        p[np.argsort(p)[:-top_n]] = 0
        p = p / np.sum(p)
        c = np.random.choice(vocab_size, 1, p=p)[0]
        return int_to_vocab[c]
    elif pred_mode == 'max':
        return int_to_vocab[probabilities.argmax()]


gen_length = 17
prime_word = ["623", "891", "262", "761", "900", "598", "306", "580", "243", "202"]

loaded_graph = tf.Graph()
with tf.Session(graph=loaded_graph) as sess:
    loader = tf.train.import_meta_graph(load_dir + '.meta')
    loader.restore(sess, load_dir)

    input_text, initial_state, final_state, probs = get_tensors(loaded_graph)
    normalized_embedding = loaded_graph.get_tensor_by_name("truediv:0")

    gen_sentences = []
    prev_state = sess.run(initial_state, {input_text: np.array([[1]])})

    x = np.zeros((1, 1))
    for word in prime_word:
        x[0, 0] = vocab_to_int[word]
        probabilities, prev_state = sess.run(
            [probs, final_state],
            {input_text: x, initial_state: prev_state})

    valid_embedding = tf.nn.embedding_lookup(normalized_embedding, probabilities.argmax())
    valid_embedding = tf.reshape(valid_embedding, (1, embed_dim))

    similarity = tf.matmul(valid_embedding, tf.transpose(normalized_embedding))
    sim = similarity.eval()

    pred_word = pick_word(probabilities, sim, int_to_vocab, 5, 'topk')  # median  topk  max  sim
    gen_sentences.append(pred_word)

    for n in range(gen_length):
        x[0, 0] = pred_word

        probabilities, prev_state = sess.run(
            [probs, final_state],
            {input_text: x, initial_state: prev_state})

        valid_embedding = tf.nn.embedding_lookup(normalized_embedding, probabilities.argmax())
        valid_embedding = tf.reshape(valid_embedding, (1, embed_dim))
        similarity = tf.matmul(valid_embedding, tf.transpose(normalized_embedding))
        sim = similarity.eval()

        pred_word = pick_word(probabilities, sim, int_to_vocab, 5, 'topk')  # median  topk  max  sim

        gen_sentences.append(pred_word)

    cp_script = ' '.join(gen_sentences)
    cp_script = cp_script.replace('\n ', '\n')
    cp_script = cp_script.replace('( ', '(')

    print(cp_script)

int_sentences = [int(words) for words in gen_sentences]
int_sentences = int_sentences[1:]

val_data = [[103],[883],[939],[36],[435],[173],[572],[828],[509],[723],[145],[621],[535],[385],[98],[321],[427]]

plt.plot(int_sentences, label='History')
plt.plot(val_data, label='val_data')
plt.legend()
_ = plt.ylim()