import os
import re

data_dir = './data/newtxt.txt'  # new_poetry
text = open(data_dir, 'rb').read().decode(encoding='utf-8')

pattern = '[a-zA-Z0-9’"#$%&\'()*+-./:;<=>@★…【】《》“”‘’[\\]^_`{|}~]+'

def preprocess_poetry(outdir, datadir):
    with open(os.path.join(outdir, 'new_poetry.txt'), 'w') as out_f:
        with open(os.path.join(datadir, 'poetry.txt'), 'r') as f:
            for line in f:
                content = line.strip().rstrip('\n').split(":")[1] #.rstrip("\n")
                content = content.replace(' ', '')

                #处理部分中文符号
                if '】' in content or '_' in content or '(' in content or '（' in content or '《' in content or '[' in content:
                    continue
                if len(content) < 20:
                    continue

                content = re.sub(pattern, '', content)
                out_f.write(content+"\n")

preprocess_poetry("./data/", "./data/")

import numpy as np
from collections import Counter
import pickle


def create_lookup_tables():
    vocab = sorted(set(text))
    vocab_to_int = {u: i for i, u in enumerate(vocab)}
    int_to_vocab = np.array(vocab)

    int_text = np.array([vocab_to_int[word] for word in text if word != '\n'])

    pickle.dump((int_text, vocab_to_int, int_to_vocab), open('preprocess.p', 'wb'))

create_lookup_tables()

import numpy as np
# 读取保存的数据
int_text, vocab_to_int, int_to_vocab = pickle.load(open('preprocess.p', mode='rb'))

def get_batches(int_text, batch_size, seq_length):

    batchCnt = len(int_text) // (batch_size * seq_length)
    int_text_inputs = int_text[:batchCnt * (batch_size * seq_length)]
    int_text_targets = int_text[1:batchCnt * (batch_size * seq_length)+1]

    result_list = []
    x = np.array(int_text_inputs).reshape(1, batch_size, -1)
    y = np.array(int_text_targets).reshape(1, batch_size, -1)

    x_new = np.dsplit(x, batchCnt)
    y_new = np.dsplit(y, batchCnt)

    for ii in range(batchCnt):
        x_list = []
        x_list.append(x_new[ii][0])
        x_list.append(y_new[ii][0])
        result_list.append(x_list)

    return np.array(result_list)

vocab_size = len(int_to_vocab)

# 批次大小
batch_size = 32  # 64
# RNN的大小（隐藏节点的维度）
rnn_size = 1000
# 嵌入层的维度
embed_dim = 256  # 这里做了调整，跟彩票预测的也不同了
# 序列的长度
seq_length = 15  # 注意这里已经不是1了，在古诗预测里面这个数值可以大一些，比如100也可以的

save_dir = './save'

import tensorflow as tf
import datetime
from tensorflow import keras
from tensorflow.python.ops import summary_ops_v2
import time

MODEL_DIR = "./poetry_models"

train_batches = get_batches(int_text, batch_size, seq_length)
losses = {"train":[], "test": []}

class poetry_network(object):
    def __init__(self, batch_size = 32):
        self.batch_size = batch_size
        self.best_loss = 9999

        self.model = tf.keras.Sequential([
            tf.keras.layers.Embedding(vocab_size, embed_dim, batch_input_shape=[batch_size, None]),
            tf.keras.layers.LSTM(rnn_size, return_sequences=True, stateful=True, recurrent_initializer="glorot_uniform"),
            tf.keras.layers.Dense(vocab_size)
        ])
        self.model.summary()

        self.optimizer = tf.keras.optimizers.Adam()
        self.ComputeLoss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

        if tf.io.gfile.exists(MODEL_DIR):
            pass
        else:
            tf.io.gfile.makedirs(MODEL_DIR)

        train_dir = os.path.join(MODEL_DIR, "summaries", "train")
        self.train_summary_writer = summary_ops_v2.create_file_writer(train_dir, flush_millis=10000)

        checkpoint_dir = os.path.join(MODEL_DIR, "checkpoints")
        self.checkpoint_prefix = os.path.join(checkpoint_dir, 'ckpt')
        self.checkpoint = tf.train.Checkpoint(model=self.model, optimizer=self.optimizer)

        #Restore variable on creation if a checkpoint exists
        self.checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

    @tf.function
    def train_step(self, x, y):
        # 计算损失函数，使用交叉熵损失计算梯度
        with tf.GradientTape() as tape:
            logits = self.model(x, training = True)
            loss = self.ComputeLoss(y, logits)

        grads = tape.gradient(loss, self.model.trainable_variables)
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))
        return loss, logits

    def training(self, epochs = 1, log_freq = 50):
        batchCnt = len(int_text)
        print("batchCnt : ", batchCnt)
        for i in range(epochs):
            train_start = time.time()
            with self.train_summary_writer.as_default():
                start = time.time()
                # Metrics are stateful. They accumulate values and return a cumulative
                # result when you call .result(). Clear accumulated values with .reset_states()
                avg_loss = tf.keras.metrics.Mean('loss', dtype=tf.float32)

                # Datasets can be iterated over like any other Python iterable.
                for batch_i, (x, y) in enumerate(train_batches):
                    loss, logits = self.train_step(x, y)
                    avg_loss(loss)
                    losses["train"].append(loss)

                    if tf.equal(self.optimizer.iterations % log_freq, 0):
                        summary_ops_v2.scalar("loss", avg_loss.result(), step=self.optimizer.iterations)

                        rate = log_freq / (time.time() - start)
                        print('Step #{}\tLoss: {:0.6f} ({} steps/sec)'.format(
                            self.optimizer.iterations.numpy(), loss, rate))
                        avg_loss.reset_states()

                        start = time.time()

            self.checkpoint.save(self.checkpoint_prefix)
            print("save model\n")

net = poetry_network()
net.training(20)

# import seaborn as sns
import matplotlib.pyplot as plt

restore_net=poetry_network(1)
restore_net.model.build(tf.TensorShape([1, None]))

#prime_word 是开始的头一个字。top_n 从前N个候选汉字中随机选择rule 默认是7言绝句sentence_lines 生成几句古诗，默认是4句（，和。都算一句）hidden_head 藏头诗的前几个字
def gen_poetry(prime_word='白', top_n=5, rule=7, sentence_lines=4, hidden_head=None):
    #计算诗词生成的单词长度
    gen_length = sentence_lines * (rule + 1) - len(prime_word)
    #判断是否要藏头诗
    gen_sentences = [prime_word] if hidden_head == None else [hidden_head[0]]
    temperature = 1.0

    #输入vocab的向量
    dyn_input = [vocab_to_int[s] for s in prime_word]
    dyn_input = tf.expand_dims(dyn_input, 0)

    dyn_seq_length = len(dyn_input[0])

    restore_net.model.reset_states()
    index = len(prime_word) if hidden_head==None else 1
    for n in range(gen_length):
        index += 1
        predictions = restore_net.model(np.array(dyn_input))
        predictions = tf.squeeze(predictions, 0)

        if index != 0 and (index % (rule + 1)) == 0:
            #判断是偶数句，还是奇数句
            if ((index / (rule + 1)) + 1) % 2 == 0:
                predicted_id = vocab_to_int['，']
            else:
                predicted_id = vocab_to_int['。']
        else:
            #如果是藏头诗，将每句的藏头设为首词
            if hidden_head != None and (index-1) % (rule + 1) == 0 and (index-1)//(rule+1) < len(hidden_head):
                predicted_id = vocab_to_int[hidden_head[(index-1)//(rule+1)]]
            else:
                #根据预测的向量进行词的生成查找TopK
                while True:
                    predictions = predictions / temperature
                    predicted_id = tf.random.categorical(predictions, num_samples=1)[-1, 0].numpy()

                    # p = np.squeeze(predictions[-1].numpy())
                    # p[np.argsort(p)[:-top_n]] = 0
                    # p = p / np.sum(p)
                    # c = np.random.choice(vocab_size, 1, p=p)[0]
                    # predicted_id=c
                    #判断预测为结束时循环退出
                    if (predicted_id != vocab_to_int['，'] and predicted_id != vocab_to_int['。']):
                        break
    # using a multinomial distribution to predict the word returned by the model
        #         predictions = predictions / temperature
        #         predicted_id = tf.multinomial(predictions, num_samples=1)[-1,0].numpy()

        dyn_input = tf.expand_dims([predicted_id], 0)
        gen_sentences.append(int_to_vocab[predicted_id])

    poetry_script = ' '.join(gen_sentences)
    poetry_script = poetry_script.replace('\n ', '\n ')
    poetry_script = poetry_script.replace('( ', '(')

    return poetry_script

poetry_script = gen_poetry(prime_word='白日依山尽', top_n=10, rule=5, sentence_lines=4)
print(poetry_script)