# coding=utf-8

# 词性预测
import numpy as np


class PosTagger():
    def __init__(self):
        self.tag2id, self.id2tag = {}, {}  # map tag to id .tag2id:{"VB": 0, "NNP":1, ..}, id2tag:{0:"VB", 1:"NNP"}
        self.word2id, self.id2word = {}, {}  # map word to id

        for line in open("./data/traindata.txt"):
            items = line.split("/")
            word, tag = items[0], items[1].rstrip()  # 抽取每一行的单词和词性

            # 将单词word转换为索引的方式
            if word not in self.word2id:
                self.id2word[len(self.word2id)] = word
                self.word2id[word] = len(self.word2id)

            # 将词性也转换成索引的方式
            if tag not in self.tag2id:
                self.id2tag[len(self.tag2id)] = tag
                self.tag2id[tag] = len(self.tag2id)

        self.M = len(self.word2id)  # 词典的大小 # of words in dictionary
        self.N = len(self.tag2id)  # 词性的种类个数 # of tag in tag set

        self.pi = np.zeros(self.N)  # 每个词性出现在句子开始位置的概率 N of tags
        self.A = np.zeros((self.N, self.M))  # A[i][j] 给定tag i ,出现单词j的概率。 N # of tag M: of word in dictionary
        self.B = np.zeros((self.N, self.N))  # B[i][j] :之前的状态是i, 之后转换成状态j的概率 N # of tag

    def initData(self):
        prev_tag = ""
        for line in open("./data/traindata.txt"):
            items = line.split('/')
            wordId, tagId = self.word2id[items[0]], self.tag2id[items[1].rstrip()]
            # 统计句子中各个词性的数目
            if prev_tag == "":  # 这意味着是句子的开始
                self.pi[tagId] += 1
                self.A[tagId][wordId] += 1
            else:
                self.B[tagId][self.tag2id[prev_tag]] += 1
                self.A[tagId][wordId] += 1

            if prev_tag == ".":
                prev_tag = ""
            else:
                prev_tag = items[1].rstrip()

        # normalize 获得概率
        self.pi /= sum(self.pi)
        for i in range(self.N):
            self.A[i] /= sum(self.A[i])
            self.B[i] /= sum(self.B[i])

        # 到此位置计算完了模型的所有参数: pi, A, B

    def viterbiAlg(self, x):
        """
        x: user input string/sentence: x: "I like playing soccer"
        pi: initial probability of tags
        A: 给定tag, 每个单词出现的概率
        B: tag之间的转移概率
        """

        def log(v):
            if v == 0:
                return np.log(v + 0.000001)
            return np.log(v)
        x = [self.word2id[word] for word in x.split(" ")]
        T = len(x)

        dp = np.zeros((T, self.N))  # dp[i][j]: w1...wi, 假设wi的tag是第j个tag
        pointArr = np.array([[0 for x in range(self.N)] for y in range(T)])
        # TODO：pointArr =

        for j in range(self.N):
            dp[0][j] = log(self.pi[j]) + log(self.A[j][x[0]])

        for i in range(1, T):  # 每个单词
            for j in range(self.N):  # 每个词性
                # TODO:以下几行代码可以一行（vectorize的操作 会效率更高一点）
                dp[i][j] = -9999999
                for k in range(self.N):  # 从每一个k可以到达j
                    score = dp[i - 1][k] + log(self.B[k][j]) + log(self.A[j][x[i]])
                    if score > dp[i][j]:
                        dp[i][j] = score
                        pointArr[i][j] = k

        # decoding: 把最好的tag sequence 打印出来
        best_seq = [0] * T  # best_seq = [1,5,2,23,4,....]
        # step1 找出对应于最后一个单词的词性
        best_seq[T - 1] = np.argmax(dp[T - 1])

        # step2:通过从后向前的循环找到每个单词的词性
        for i in range(T - 2, -1, -1):
            best_seq[i] = pointArr[i + 1][best_seq[i + 1]]

        # 到目前为止, best_seq存放了对应于x的 词性序列
        for i in range(len(best_seq)):
            print(self.id2tag[best_seq[i]])


sys = PosTagger()
sys.initData()
x = "Social Security number , passport number and details about the services provided for the payment"
sys.viterbiAlg(x)
