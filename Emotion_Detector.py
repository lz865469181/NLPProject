import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn. model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix


#Reading the Dataset(ISEAR Dataset)
data = pd.read_csv("./data/ISEAR.csv", header=None)
print(data.head())

#split the train set
labels = data[0].values.tolist()
sents = data[1].values.tolist()

X_train,X_test,y_train,y_test = train_test_split(sents, labels, test_size=0.2, random_state=42)

#Feature extraction
vectorizer = TfidfVectorizer()
#vectorizer = CountVectorizer()
X_train = vectorizer.fit_transform(X_train)
X_test = vectorizer.transform(X_test)

print(X_train)
#其他特征可以提取
#词性的特征
#n-gram

#Grid Search 的参数列表
parameters = {'C':[0.00001, 0.0001, 0.001, 0.01, 0.05, 0.1, 0.5, 1, 2, 5, 10]}
lr = LogisticRegression()
#逻辑回归实现
lr.fit(X_train, y_train).score(X_test, y_test)

clf = GridSearchCV(lr, parameters, cv=10)
#参数搜索
clf.fit(X_train, y_train)
clf.score(X_test, y_test)
print(clf.best_params_)

#混淆矩阵
confusion_matrix(y_test, clf.predict(X_test))





