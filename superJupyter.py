#!/usr/bin/env python
# coding: utf-8

# ### lambda 表达式
# #### 又被称之为匿名函数
# #### 格式 lambda 参数列表:函数体

# In[1]:


def add(x, y):
    return x + y
print(add(3, 4))


# In[2]:


add_lambda = lambda x,y:x+y
print(add_lambda(5, 2))


# ### 三元运算符

# In[3]:


condition = True
print(1 if condition else 2)


# In[4]:


condition = False
print(1 if condition else 2)


# ### map函数的应用

# In[6]:


req_1 = [1,2,3,4, 5]
r = map(lambda x: x + x, req_1)
r = list(r)
print(r)


# In[7]:


m1 = map(lambda x,y: x*x + y, [1,4,5,6],[1,2,3,4])
print(list(m1))


# ### filter 过滤器

# In[9]:


def is_notNone(s):
    return s and len(s.strip()) > 0

seq2 = [' ', '','hello','greedy', None, 'ai']
result = filter(is_notNone, seq2)
print(list(result))


# ### reduce函数

# In[10]:


from functools import reduce
f = lambda x, y :x + y
r = reduce(f, [1,2,3,4,5])
print(r)


# ### 列表推导式
# 根据已有的列表推导出新的列表

# In[11]:


list1 = [1,2,3,4,5]
list2 = [i+i for i in list1]
print(list2)


# In[12]:


list3= [i**3 for i in list1]
print(list3)


# In[15]:


#有选择的筛选
list4 = [i*i for i in list1 if i > 2]
print(list4)


# ### 集合推导式

# In[19]:


list1 = {1,2,3,4,5}
list2 = {i+i for i in list1}
print(list2)

list3= {i**3 for i in list1}
print(list3)


# In[20]:


#有选择的筛选
list4 = {i*i for i in list1 if i > 2}
print(list4)


# ### 字典推导式

# In[22]:


s = {
    "zhangsan":20,
    "list":15,
    "wangwu":31
}
#拿出所有的key，并变成列表
s_key = [key for key, value in s.items()]
print(s_key)


# In[25]:


#key和value 颠倒
s1 = {value:key for key, value in s.items()}
print(s1)


# In[26]:


s1 = {value:key for key, value in s.items() if key == 'list'}
print(s1)


# ### 闭包：一个返回值是函数的函数

# In[ ]:




