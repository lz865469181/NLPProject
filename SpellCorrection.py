#coding=utf-8
from nltk.corpus import reuters
import numpy as np

#词典库获取
class SpellCorrection():
    def __init__(self):
        self.vocab = set([line.rstrip() for line in open('./data/spellcor/vocab.txt')])
    '''
    需要生成所有候选词集合
    '''

    def generate_candidates(self,wrong_word):
        """
        word: 给定的输入（错误的输入）
        返回所有(valid)候选集合
        """
        # 生成编辑距离为1的单词
        # 1.insert 2. delete 3. replace
        # appl: replace: bppl, cppl, aapl, abpl...
        #       insert: bappl, cappl, abppl, acppl....
        #       delete: ppl, apl, app

        letters = 'abcdefghijklmnopqrstuvwxyz'

        splits = [(wrong_word[:i], wrong_word[i:]) for i in range(len(wrong_word) + 1)]
        inserts = [left + letter + right for left, right in splits for letter in letters]
        deletes = [left + right[1:] for left, right in splits]
        replaces = [left + letter + right[1:] for left, right in splits for letter in letters]

        candidates = set(inserts + deletes + replaces)

        # 过滤掉不存在于词典库里面的单词
        return [candi for candi in candidates if candi in self.vocab]

    # 生成编辑距离为2的单词
    def generate_edit_two(self, wrong_word):
        def generate_edit_one(wrong_word):
            letters = 'abcdefghijklmnopqrstuvwxyz'
            splits = [(wrong_word[:i], wrong_word[i:]) for i in range(len(wrong_word) + 1)]
            inserts = [left + letter + right for left, right in splits for letter in letters]
            deletes = [left + right[1:] for left, right in splits]
            replaces = [left + letter + right[1:] for left, right in splits for letter in letters]
            return set(inserts + deletes + replaces)

        candi_one = generate_edit_one(wrong_word)
        candi_list = []
        for candi in candi_one:
            candi_list.extend(generate_edit_one(candi))
        candi_two = set(candi_list)
        return [candi for candi in candi_two if candi in self.vocab]

    def generate_grammodel(self):
        # 读取语料库
        categories = reuters.categories()
        corpus = reuters.sents(categories=categories)

        #构建语言模型 biggram
        self.term_count = {}
        self.biggram_count = {}
        for doc in corpus:
            doc = ['<s>'] + doc
            for i in range(0, len(doc) - 1):
                #biggram
                term = doc[i]
                biggram = doc[i:i+2]

                if term in self.term_count:
                    self.term_count[term] += 1
                else:
                    self.term_count[term] = 1

                biggram = ' '.join(biggram)
                if biggram in self.biggram_count:
                    self.biggram_count[biggram] += 1
                else:
                    self.biggram_count[biggram] = 1

        self.misspell_prob = {}

        for line in open('./data/spellcor/spell-errors.txt'):
            items = line.split(":")
            correct = items[0].strip()
            miscakes = [word.strip() for word in items[1].strip().split(",")]
            self.misspell_prob[correct] = {}
            for mis in miscakes:
                self.misspell_prob[correct][mis] = 1.0/len(miscakes)

        #print(self.misspell_prob)


    def spellText(self):
        V = len(self.term_count.keys())
        with open('./data/spellcor/testdata.txt', 'r') as file:
            lines = file.readlines()
            for line in lines:
                item = line.rstrip().split('\t')
                word_list = item[2].split()

                for index, word in enumerate(word_list):
                    word = word.strip(",.")
                    if word not in self.vocab:
                        #需要替换word 成正确的单词
                        #step1 生成所有的valid候选集合
                        candidates = self.generate_candidates(word)
                        if len(candidates) == 0:
                            candidates = self.generate_edit_two(word)
                        # 一种方式： if candidate = [], 多生成几个candidates, 比如生成编辑距离不大于2的
                        # TODO ： 根据条件生成更多的候选集合
                        probs = []
                        prob_dict = {}
                        # 对于每一个candidate, 计算它的score
                        # score = p(correct)*p(mistake|correct)
                        #       = log p(correct) + log p(mistake|correct)
                        # 返回score最大的candidate
                        for candi in candidates:
                            prob = 0
                            #a 计算misspell probablity 计算log p(mistake|correct)
                            if candi in self.misspell_prob and word in self.misspell_prob[candi]:
                                prob += np.log(self.misspell_prob[candi][word])
                            else:
                                prob += np.log(0.0001)

                            # b. log p(correct)，计算计算过程中使用了Add-one Smoothing的平滑操作
                            # 先计算log p(word|pre_word)
                            pre_word = word_list[index - 1] if index > 0 else '<s>'
                            bigram_pre = ' '.join([pre_word, word])
                            #计算bigram的概率显示
                            if pre_word in self.term_count and bigram_pre in self.biggram_count:
                                prob += np.log((self.biggram_count[bigram_pre] + 1) / (self.term_count[pre_word]) + V)
                            elif pre_word in self.term_count:
                                prob += np.log(1/(self.term_count[pre_word] + V))
                            else:
                                prob += np.log(1/V)

                            #在计算log p(next_word|word)
                            if index + 1 < len(word_list):
                                next_word = word_list[index + 1]
                                bigram_next = ' '.join([word, next_word])
                                if word in self.term_count and bigram_next in self.biggram_count:
                                    prob += np.log((self.biggram_count[bigram_next] + 1) / (self.term_count[word] + V))
                                elif word in self.term_count:
                                    prob += np.log(1/(self.term_count[word] + V))
                                else:
                                    prob += np.log(1/V)

                            probs.append(prob)
                            prob_dict[candi] = prob

                        if probs:
                            max_idx = probs.index(max(probs))
                            print(word, candidates[max_idx])
                            print(prob_dict)
                        else:
                            print(word, False)



sys = SpellCorrection()
sys.generate_grammodel()
sys.spellText()

