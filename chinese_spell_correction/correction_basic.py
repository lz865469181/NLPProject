# @Author:Luzhou
# @Time  : 2021/02/24

"""
利用语言模型提供的置信度+字音和字形生成的相似度,加权输出一个分数,选取分数最高的候选字符
模型参数链接 https://github.com/google-research/bert
模型下载地址 https://storage.googleapis.com/bert_models/2018_11_03/chinese_L-12_H-768_A-12.zip
"""

import json
from bert4keras.tokenizers import load_vocab, Tokenizer
from bert4