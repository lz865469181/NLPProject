import warnings
warnings.filterwarnings("ignore")

import numpy as np
import pandas as pd

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from gensim.models import Word2Vec
import nltk

data_path = "./data/Reviews.csv"
data = pd.read_csv(data_path)
data_sel = data.head(1000)
#print(data_sel)

#shape of our data
print(data_sel.columns)
print(data_sel['Score'])

#Neutral reviews removed
data_score_removed = data_sel[data_sel['Score'] != 3]

#Covert Score Value into class label
def partition(x):
    if x < 3:
        return 'positive'
    return 'negative'

socre_upd = data_score_removed['Score']
t = socre_upd.map(partition)
data_score_removed['Score']=t

final_data = data_score_removed.drop_duplicates(subset={"UserId", "ProfileName", "Time", "Text"})
final = final_data[final_data['HelpfulnessNumerator'] <= final_data['HelpfulnessDenominator']]

final_X = final['Text']
final_Y = final['Score']

stop = set(stopwords.words('english'))
print(stop)

import re
temp = []
snow = nltk.stem.SnowballStemmer('english')
for sentence in final_X:
    sentence = sentence.lower() # Converting to lowercase
    cleaner = re.compile('<.*?>')
    sentence = re.sub(cleaner, ' ', sentence)  # Removing HTML tags
    sentence = re.sub(r'[?|!|\'|"|#]', r'', sentence)
    sentence = re.sub(r'[.|,|)|(|\|/]', r' ', sentence)  # Removing Punctuations

    words = [snow.stem(word) for word in sentence.split() if word not in stopwords.words('english')] # Stemming and removing stopwords
    temp.append(words)

final_X = temp
print(final_X[1])

sent = []
for row in final_X:
    sequ = ''
    for word in row:
        sequ = sequ + ' ' + word
    sent.append(sequ)

final_X = sent
print(final_X[1])

count_vect = CountVectorizer(max_features=5000)
bow_data = count_vect.fit_transform(final_X)
print(bow_data[1])

final_B_X = final_X

count_vect = CountVectorizer(ngram_range=(1,2))
Bigram_data = count_vect.fit_transform(final_B_X)
print(Bigram_data[1])

final_tf = final_X
tf_idf = TfidfVectorizer(max_features=5000)
tf_data = tf_idf.fit_transform(final_tf)
print(tf_data[1])

w2v_data = final_X

splitted = []
for row in w2v_data:
    splitted.append([word for word in row.split()])  #splitting words

train_w2v = Word2Vec(splitted, min_count=5, size=50, workers=4)

avg_data = []
for row in splitted:
    vec = np.zeros(50)
    count = 0
    for word in row:
        try:
            vec += train_w2v[word]
            count += 1
        except:
           pass
    avg_data.append(vec/count)

print(avg_data[1])

tf_w_data = final_X
tf_idf = TfidfVectorizer(max_features=5000)
tf_idf_data = tf_idf.fit_transform(tf_w_data)

tf_w_data = []
tf_idf_data = tf_idf_data.toarray()
i = 0
for row in splitted:
    vec = [0 for i in range(50)]

    temp_tfidf = []
    for val in tf_idf_data[i]:
        if val != 0:
            temp_tfidf.append(val)

    count = 0
    tf_idf_sum = 0
    for word in row:
        try:
            count += 1
            tf_idf_sum = tf_idf_sum + temp_tfidf[count - 1]
            vec += (temp_tfidf[count - 1] * train_w2v[word])
        except:
            pass
    vec = (float)(1 / tf_idf_sum) * vec
    tf_w_data.append(vec)
    i = i + 1

print(tf_w_data[1])