#coding=utf-8
from openpyxl import load_workbook
import jieba

#基于动态规划的解法
def edit_dist(str1, str2):
    #m,n 分别字符串str1和str2
    m, n = len(str1), len(str2)

    #构建二位数组来存储子问题(sub-problem)问题
    dp = [[0 for x in range(n+1)] for x in range(m + 1)]
    print(dp)

    #利用动态规划算法填充数组
    for i in range(m+1):
        for j in range(n+1):
            #假设第一个字符串为空，则转换的代价为j(j次的插入)
            if i == 0:
                dp[i][j] = j

            #同样的假设第二个字符串为空,则转换代价为i(i次的插入)
            elif j == 0:
                dp[i][j] = i
            #如果最后一个字符相等，就不会产生代价
            elif str1[i - 1] == str2[j - 1]:
                dp[i][j] = dp[i-1][j-1]

            #如果最后一个字符不一样则考虑多种可能性,并且选择其中最小值
            else:
                dp[i][j] = 1 + min(dp[i][j-1],
                                   dp[i-1][j],
                                   dp[i-1][j-1])
    return  dp[m][n]

def generate_edit_one(str):
    """
    给定一个字符串，生成编辑距离为1的字符串列表
    """
    letters = 'abcdefghijklmnopqrstuvwxyz'
    splits = [(str[:i], str[i:]) for i in range(len(str) + 1)]
    inserts = [L + C + R for L,R in splits for C in letters]
    deletes = [L + R[1:] for L,R in splits if R]
    replaces = [L + C + R[1:] for L, R in splits if R for C in letters]

    return set(inserts + deletes + replaces)

def generate_edit_two(str):
    """
    给定一个字符串，生成编辑距离不大于2的字符串
    """
    return [e2 for e1 in generate_edit_one(str) for e2 in generate_edit_one(e1)]

def jiebacut():
    #基于jieba的分词
    seg_list = jieba.cut("贪心学院专注于人工智能教育", cut_all=False, HMM=False)
    print("Default Mode:" + "/".join(seg_list))

    jieba.add_word("贪心学院")
    seg_list = jieba.cut("贪心学院专注于人工智能教育", cut_all=False)
    print("Default Mode:" + "/".join(seg_list))

def word_break(str):
    #判断一句话是否能切分(被字典)
    dic = set(["贪心科技", "人工智能", "教育", "在线", "专注于"])
    could_break = [False] * (len(str) + 1)
    could_break[0] = True

    for i in range(1, len(could_break)):
        for j in range(0, i):
            if str[j:i] in dic and could_break[j] == True:
                print(str[j:i])
                could_break[i] = True

    return could_break[len(str)] == True

def all_possible_segmentations(str):
    segs = []
    dic = set(["贪心科技", "人工智能", "教育", "在线", "专注于", "贪心"])

    for i in range(1, len(str)+1):
        for j in range(0, i):
            if str[j:i] in dic:
                segs.append(str[j:i])

    return segs
def stop_words():
    #方法 自己建立一个停用词词典
    stop_words = ["the", "an", "is", "there", "are"]
    #在使用时 假设 word_list包含了文本中的单词
    word_list = ["we", "are", "the", "students"]

    filtered_words = [word for word in word_list if word not in stop_words]
    print(filtered_words)

    from nltk.corpus import stopwords
    from nltk.stem.porter import *
    from sklearn.feature_extraction.text import CountVectorizer
    stemmer = PorterStemmer()
    test_strs = ['caresses', 'flies', 'dies', 'mules', 'denied',
                 'died', 'agreed', 'owned', 'humbled', 'sized',
                 'meeting', 'stating', 'siezing', 'itemization',
                 'sensational', 'traditional', 'reference', 'colonizer',
                 'plotted']

    singles = [stemmer.stem(word) for word in test_strs]
    print(' '.join(singles))

    cachedStopedWordds = stopwords.words("english")
    print(cachedStopedWordds)

    vectorizer = CountVectorizer()
    corpus = [
        'He is going from Beijing to Shanghai.',
        'He denied my request, but he actually lied.',
        'Mike lost the phone, and phone was in the car.',
    ]
    X = vectorizer.fit_transform(corpus)
    print(X.toarray())
    print(vectorizer.get_feature_names())

    from sklearn.feature_extraction.text import TfidfVectorizer
    vectorizer1 = TfidfVectorizer(smooth_idf=False)
    X1 = vectorizer1.fit_transform(vectorizer1)
    print(X1.toarray())
    print(vectorizer1.get_feature_names())



if __name__ == '__main__':
    #edit_dist('geek', 'gesek')
    #print(generate_edit_one("apple"))
    #jiebacut()
    #print(word_break("贪心科技在线教育"))
    #assert word_break("在线教育是") == False
    #assert word_break("") == True
    #assert word_break("在线教育人工智能") == True
    #all_possible_segmentations("贪心科技在线教育")
    stop_words()

