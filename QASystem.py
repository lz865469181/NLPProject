#coding=utf-8
import json
from collections import Counter
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
import string
import math
from sklearn.feature_extraction.text import TfidfVectorizer
from queue import PriorityQueue
from collections import defaultdict
from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec
import numpy as np

##
# 搭建一个基于检索式的简单的问答系统。至于什么是检索式的问答系统请参考课程直播内容/PPT介绍。
# 通过此项目，你将会有机会掌握以下几个知识点： 1. 字符串操作 2. 文本预处理技术（词过滤，标准化） 3. 文本的表示（tf-idf, word2vec) 4. 文本相似度计算 5. 文本高效检索
# 此项目需要的数据： 1. dev-v2.0.json: 这个数据包含了问题和答案的pair， 但是以JSON格式存在，需要编写parser来提取出里面的问题和答案。 2. glove.6B: 这个文件需要从网上下载，下载地址为：https://nlp.stanford.edu/projects/glove/， 请使用d=100的词向量
# 检索式的问答系统
# 问答系统所需要的数据已经提供，对于每一个问题都可以找得到相应的答案，所以可以理解为每一个样本数据是 <问题、答案>。 那系统的核心是当用户输入一个问题的时候，首先要找到跟这个问题最相近的已经存储在库里的问题，然后直接返回相应的答案即可。 举一个简单的例子：
# 假设我们的库里面已有存在以下几个<问题,答案>： <"贪心学院主要做什么方面的业务？”， “他们主要做人工智能方面的教育”> <“国内有哪些做人工智能教育的公司？”， “贪心学院”> <"人工智能和机器学习的关系什么？", "其实机器学习是人工智能的一个范畴，很多人工智能的应用要基于机器学习的技术"> <"人工智能最核心的语言是什么？"， ”Python“> .....
# 假设一个用户往系统中输入了问题 “贪心学院是做什么的？”， 那这时候系统先去匹配最相近的“已经存在库里的”问题。 那在这里很显然是 “贪心学院是做什么的”和“贪心学院主要做什么方面的业务？”是最相近的。 所以当我们定位到这个问题之后，直接返回它的答案 “他们主要做人工智能方面的教育”就可以了。 所以这里的核心问题可以归结为计算两个问句（query）之间的相似度。
# 在本次项目中，你会频繁地使用到sklearn这个机器学习库。具体安装请见：http://scikit-learn.org/stable/install.html sklearn包含了各类机器学习算法和数据处理工具，包括本项目需要使用的词袋模型，均可以在sklearn工具包中找得到。 另外，本项目还需要用到分词工具jieba, 具体使用方法请见 https://github.com/fxsjy/jieba
#
#https://blog.csdn.net/Divine0/article/details/106545067#comments_14169561

class QASystem(object):
    def __init__(self):
        self.status = 0

    def read_corpus(self, filepath):
        """
        读取给定的语料库，并把问题集和答题列表分别写入到qlist和alist里面。在此过程中不用对字符做任何处理
        qlist = ["问题1", "问题2"， “问题3"]
        alist = ["答案1", "答案2”， “答案3”]
        """
        with open(filepath, "r") as file:
            fileJson = json.load(file)
        qlist = []
        alist = []
        for dataJson in fileJson['data']:
            for grapJson in dataJson['paragraphs']:
                for qa_dict in grapJson['qas']:
                    qlist.append(qa_dict['question'])
                    try:#会出现没有答案的数据
                        alist.append(qa_dict['answers'][0]['text'])
                    except IndexError:
                        qlist.pop()

        assert len(qlist) == len(alist) #判断问答对是否一一对应，如果没有对应说明不是正确的数据
        return qlist,alist

    def countWords(self, qlist, alist):
        word_cnt = Counter() #创建计数器
        for text in qlist:
            word_cnt.update(text.strip(' .!?').split(' '))
        print(sum(word_cnt.values())) #统计所有的单词种类

        # values = []
        # wordstopq = []
        # for item in word_cnt.most_common(100):
        #     values.append(item[1])
        #     wordstopq.append(item[0])
        # plt.xlabel('word')
        # plt.ylabel('count')
        # plt.title("word cnt view")
        # plt.plot(values)
        # plt.show()
        word_freq = {}
        for question in qlist:
            question = question.replace('?', ' ?')
            line = question.strip().split()
            for word in line:
                if word in word_freq:
                    word_freq[word] += 1;
                else:
                    word_freq[word] = 1
        sorted_word_freq = sorted(word_freq.items(), key= lambda x : x[1], reverse=True)
        all_word = []
        word_cnt = []
        for word, count in sorted_word_freq:
            all_word.append(word)
            word_cnt.append(count)
        plt.bar(range(20), word_cnt[:20], color='rgb', tick_label=all_word[:20])#画出前20个词
        plt.show()

        word_freq_a = {}
        for answer in alist:
            answer = answer.replace('?', ' ?')
            line = answer.strip().split()
            for word in line:
                if word in word_freq_a:
                    word_freq_a[word] += 1;
                else:
                    word_freq_a[word] = 1
        sorted_word_freq_a = sorted(word_freq_a.items(), key=lambda x: x[1], reverse=True)

        print("qlist top 10 单词分别是：")
        for x in range(10):
            print(all_word[x])

        print("alist top 10 单词分别是：")
        for y in range(10):
            print(sorted_word_freq_a[y][0])

    #文本预处理以及进行停用词过滤和其他方法
    def preprocessing(self, words, stopwords):
        word_list= []

        token = word_tokenize(words)
        for word in token:
            #分词显示 tokenize之后 进行单负数还原到原单词
            word = self.wordStem.stem(word.lower())
            #判断是否为数字，然后转为number
            word = "#number" if word.isdigit() else word
            if word not in stopwords:
                word_list.append(word)

        return word_list

    # 根据Zipf定律保留99%的文本
    def filter_word(self, qlist_seg):
        '''
        Zipf定律：第n常见的词的出现次数是最常见的词出现次数的1/n，
        当n足够大时，数列1/n的累加约等于ln(n)，即为文本总的单词数，
        为使99%的文本得到覆盖则阈值点x需满足ln(x)=0.99*ln(n)，x=e^(0.99*ln(n))。
        '''
        value_sort = sorted(self.word_cnt.values(), reverse=True)
        ts_value = value_sort[int(math.exp(0.99 * math.log(len(self.word_cnt))))]
        for cur in range(len(qlist_seg)):
            qlist_seg[cur] = [word for word in qlist_seg[cur] if self.word_cnt[word] > ts_value]
        return qlist_seg

    def textprocess(self, qlist):
        #停用词过滤
        stop_words = set(stopwords.words('english'))
        stop_words.update(string.punctuation) #停用词去除一些符号

        self.wordStem = PorterStemmer()
        self.word_cnt = Counter()
        qlist_seq = []
        for text in qlist:
            #进行词预处理 分词，stemming tokenize
            word_list = self.preprocessing(text, stop_words)
            qlist_seq.append(word_list)
            self.word_cnt.update(word_list)
        # 过滤低频词
        qlist_seq = self.filter_word(qlist_seq)
        return  qlist_seq

    def wordVector(self, qlist):
        '''
        TODO: 把qlist中的每一个问题字符串转换成tf-idf向量, 转换之后的结果存储在X矩阵里。 X的大小是： N* D的矩阵。 这里N是问题的个数（样本个数），
        D是字典库的大小。
        '''
        self.vectorizer = TfidfVectorizer() # 定义一个tf-idf的vectorizer

        X = self.vectorizer.fit_transform([' '.join(word) for word in qlist_seq]) #向量化
        #TODO: 矩阵X有什么特点？ 计算一下它的稀疏度
        sparse = 1.000 - X.nnz / float(X.shape[0] * X.shape[1])
        print(X.shape)
        print(sparse)

        return X

    def top5results(self, input_q, X):
        """
        给定用户输入的问题 input_q, 返回最有可能的TOP 5问题。这里面需要做到以下几点：
        1. 对于用户的输入 input_q 首先做一系列的预处理，然后再转换成tf-idf向量（利用上面的vectorizer)
        2. 计算跟每个库里的问题之间的相似度
        3. 找出相似度最高的top5问题的答案
        tmp = ''
    for sign in ['.', '?', '/', '#', '$', '@', '^', '*', '!', '(', ')']:
        question = input_q.replace(sign, '')
    # question = question.replace('?', ' ?')
    line = question.strip().split()
    for word in line:
        try:
            if word_freq[word] <= 20:
                continue
            word = word.lower()
        except:
            pass
        if word in stopwords:
            continue
        for num in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
            if str(num) in word:
                word = '1'
        word = porter_stemmer.stem(word)
        tmp = tmp + word + " "
    input_str = [tmp[:-1]]
    input_str = tf_idf_model.transform(input_str).toarray()[0]

    simlarity = [0]*len(X)
    for index in range(len(X)):
        cos_sim = np.dot(input_str, X[index]) / (np.linalg.norm(input_str)*np.linalg.norm(X[index])+1)
        simlarity[index] = cos_sim

    top_idxs = []# top_idxs存放相似度最高的（存在qlist里的）问题的下表
    for _ in range(5):
        index = simlarity.index(max(simlarity))
        top_idxs.append(index)
        del simlarity[index]
        """
        stop_words = set(stopwords.words('english'))
        stop_words.update(string.punctuation)  # 停用词去除一些符号

        #将输入的文本预处理后转为向量化
        q_vector = self.vectorizer.transform([' '.join(self.preprocessing(input_q, stop_words))])
        sim = (X * q_vector.T).toarray()

        pqueue = PriorityQueue()
        for cur in range(sim.shape[0]):
            pqueue.put((sim[cur][0], cur))
            if len(pqueue.queue) > 5:
                pqueue.get()
        pq_rank = sorted(pqueue.queue,key=lambda x:x[0], reverse=True)


        top_idxs = []  # top_idxs存放相似度最高的（存在qlist里的）问题的下表
        # hint: 利用priority queue来找出top results. 思考为什么可以这么做？
        print([item[0] for item in pq_rank])
        return [alist[item[1]] for item in pq_rank]  # 返回相似度最高的问题对应的答案，作为TOP5答案

    # TODO: 基于倒排表的优化。在这里，我们可以定义一个类似于hash_map, 比如 inverted_index = {}， 然后存放包含每一个关键词的文档出现在了什么位置，
    #       也就是，通过关键词的搜索首先来判断包含这些关键词的文档（比如出现至少一个），然后对于candidates问题做相似度比较。
    #
    def top5results_invidx(self, input_q, qlist_seq, X):
        """
        给定用户输入的问题 input_q, 返回最有可能的TOP 5问题。这里面需要做到以下几点：
        1. 利用倒排表来筛选 candidate
        2. 对于用户的输入 input_q 首先做一系列的预处理，然后再转换成tf-idf向量（利用上面的vectorizer)
        3. 计算跟每个库里的问题之间的相似度
        4. 找出相似度最高的top5问题的答案
        """
        inverted_index = defaultdict(set)
        for cur in range(len(qlist_seq)):
            for word in qlist_seq[cur]:
                    inverted_index[word].add(cur)

        #预处理输入文本
        stop_words = set(stopwords.words('english'))
        stop_words.update(string.punctuation)  # 停用词去除一些符号
        seg = self.preprocessing(input_q, stop_words)
        candicate = set()
        #生成答案候选集
        for word in seg:
            candicate = candicate | inverted_index[word]
        candicate = list(candicate)

        q_vector = self.vectorizer.transform([' '.join(seg)])
        sim = (X[candicate] * q_vector.T).toarray()

        #该优先队列 插入新元素，会自动按照升序或降序进行元素排列
        pq = PriorityQueue()
        for cur in range(sim.shape[0]):
            pq.put((sim[cur][0], candicate[cur]))
            if (len(pq.queue) > 5):
                pq.get()

        pq_rank = sorted(pq.queue, key=lambda x : x[0], reverse=True)
        return [alist[item[1]] for item in pq_rank]

    # 读取每一个单词的嵌入。这个是 D*H的矩阵，这里的D是词典库的大小， H是词向量的大小。 这里面我们给定的每个单词的词向量，那句子向量怎么表达？
    # 其中，最简单的方式 句子向量 = 词向量的平均（出现在问句里的）， 如果给定的词没有出现在词典库里，则忽略掉这个词。

    def get_wordvec(self, word_list):
        vector = np.zeros((1, 100))
        size = len(word_list)
        for word in word_list:
            try:
                vector += self.word2vecModel[word]
            except KeyError:
                size -= 1
        if size > 0:
            return vector / size
        else:
            return vector

    def top5results_emb(self, input_q, qlist_seq):
        """
        param :input_q :输入的问题
        param :k:要包含问题的前K个字符，依次来对问题库进行过滤， k 越大要求越严格
        return : 返回相似度Top2的答案
        给定用户输入的问题 input_q, 返回最有可能的TOP 5问题。这里面需要做到以下几点：
        1. 利用倒排表来筛选 candidate
        2. 对于用户的输入 input_q，转换成句子向量
        3. 计算跟每个库里的问题之间的相似度
        4. 找出相度最高的top5问题的答案
        """
        embedding = {}
        glove2word2vec('./data/glove.6B.100d.txt', './data/glove2word2vec.6B.100d.txt')
        self.word2vecModel = KeyedVectors.load_word2vec_format('./data/glove2word2vec.6B.100d.txt')

        X = np.zeros((len(qlist_seq), 100))
        for cur in qlist_seq:
            X[cur] = self.get_wordvec(qlist_seq[cur])
        #然后对X归一化
        X = X / np.linalg.norm(X, axis=1, keepdims=True)

        inverted_index = defaultdict(set)
        for cur in range(len(qlist_seq)):
            for word in qlist_seq[cur]:
                inverted_index[word].add(cur)

        # 预处理输入文本
        stop_words = set(stopwords.words('english'))
        stop_words.update(string.punctuation)  # 停用词去除一些符号
        seg = self.preprocessing(input_q, stop_words)
        candicate = set()
        for word in seg:
            candicate = candicate | inverted_index[word]
        candicate = list(candicate)

        #使用Glove获得句子向量
        q_vector = self.get_wordvec(seg)
        q_vector = q_vector / np.linalg.norm(q_vector, axis=1, keepdims=True)

        #计算所有的相似度
        sim = X[candicate] @ q_vector.T

        pq = PriorityQueue()
        for cur in range(sim.shape[0]):
            pq.put((sim[cur][0], cur))
            if (len(pq.queue) > 5):
                pq.get()

        pq_rank = sorted(pq.queue, key= lambda x : x[0], reverse=True)
        return [alist[item[1]] for item in pq_rank]

sys = QASystem()
qlist,alist = sys.read_corpus("./data/train-v2.0.json")
#sys.countWords(qlist, alist)
qlist_seq = sys.textprocess(qlist)
X = sys.wordVector(qlist_seq)
print("使用Top5进行简易问答系统：\n")
print(sys.top5results("Which airport was shut down?", X))    # 在问题库中存在，经过对比，返回的首结果正确
print(sys.top5results("Which airport is closed?", X))
print(sys.top5results("What government blocked aid after Cyclone Nargis?", X))    # 在问题库中存在，经过对比，返回的首结果正确
print(sys.top5results("Which government stopped aid after Hurricane Nargis?", X))
print("使用倒排表方式进行简易问答系统: \n")
print(sys.top5results_invidx("Which airport was shut down?", qlist_seq, X))  # 在问题库中存在，经过对比，返回的首结果正确
print(sys.top5results_invidx("Which airport is closed?", qlist_seq, X))
print(sys.top5results_invidx("What government blocked aid after Cyclone Nargis?", qlist_seq, X))  # 在问题库中存在，经过对比，返回的首结果正确
print(sys.top5results_invidx("Which government stopped aid after Hurricane Nargis?", qlist_seq, X))

print("使用Glove进行简易问答系统: \n")
print(sys.top5results_emb("Which airport was shut down?", qlist_seq))  # 在问题库中存在，经过对比，返回的首结果正确
print(sys.top5results_emb("Which airport is closed?", qlist_seq))
print(sys.top5results_emb("What government blocked aid after Cyclone Nargis?", qlist_seq))  # 在问题库中存在，经过对比，返回的首结果正确
print(sys.top5results_emb("Which government stopped aid after Hurricane Nargis?", qlist_seq))
