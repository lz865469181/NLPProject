##
## skip-gram code
##数据预处理
##构建损失器及网络
##模型训练
##
#导入包
import numpy as np
import torch
from torch import nn, optim
import random
from collections import Counter

#读取文件
file_path = ""
with open(file_path) as f:
    text = f.read()

#预处理
def preprocess(text, freq=1):
    text = text.lower()
    text = text.replace(".", "<PERIO>")
    #省略处理特殊字符

    words = text.split()

    word_counts = Counter(words) #每个元素都是单词
    trimmed_words = [word for word in words if word_counts[word] > freq]

    return trimmed_words

#准备工作
#词典准备好，文本转为数字，训练样本准备
words = preprocess(text)
vocab = set(words)
vocab2int= {w:c for c,w in enumerate(list(vocab))}
int2vocab = {w:c for c,w in enumerate(list(vocab))}

t = 1e-5
int_words = [vocab2int[w] for w in words]
int_words_counts = Counter(int_words)
total_count = len(int_words)
word_freq = {w:c/total_count for w,c in int_words_counts.items()}
prob_drop = {w: 1- np.sqrt(t/word_freq[w]) for w in int_words_counts}
train_words = [w for w in int_words if random.random() < (1-prob_drop[w])]

#获取周边词
def get_target(words, idx, window_size):
    target_window = np.random.randint(1, window_size)
    start_point = idx - target_window if (idx - target_window) > 0 else 0
    end_point = idx + target_window

    targets = set(words[start_point:idx] + words[idx+1:end_point])
    return list(targets)

#训练数据batch迭代器
def get_batch(words, batch_size, window_size):
    n_batches = len(words) // batch_size
    words = words[:n_batches*batch_size]
    for idx in range(0, len(words), batch_size):
        batch_x, batch_y = [], []
        batch = words[idx:idx+batch_size]
        for i in range(len(batch)):
            x = batch[i]
            y = get_batch(batch, 1, window_size)
            batch_x.append([x]*len(y))
            batch_y.append(y)
            yield batch_x, batch_y

#构造具体的网络
class SkipGramNeg(nn.Module):
    def __init__(self, n_vocab, n_embed, noise_dist=None):
        super.__init__()

        self.n_vocab = n_vocab
        self.n_embed = n_embed
        self.noise_dist= noise_dist

        #define embedding layer for input and output words
        self.in_embed = nn.Embedding(n_vocab, n_embed)
        self.out_embed = nn.Embedding(n_vocab, n_embed)

        #Initialize embedding tables with uniform distribution
        self.in_embed.weight.data.uniform_(-1, 1)
        self.out_embed.weight.data.uniform_(-1, 1)

    def forword_input(self, input_words):
         input_vectors = self.in_embed(input_words)
         return input_vectors

    def forword_output(self, output_words):
        output_vectors = self.out_embed(output_words)
        return output_vectors

    def forword_noise(self, batch_size, n_samples):
        """"""
        if self.noise_dist is None:
            #sample owrds uniformly
            noise_dist = torch.ones(self.n_vocab)
        else:
            noise_dist = self.noise_dist

        #sample words form our noise distribution
        noise_words = torch.multinomial(noise_dist, batch_size*n_samples, replacement=True)
        noise_vector = self.out_embed(noise_words).view(batch_size, n_samples, self.n_embed)
        return noise_vector

word_freq = np.array(word_freq.values())
unigram_dist = word_freq/word_freq.sum()
noise_dist = torch.from_numpy(unigram_dist**(0.75)/np.sum(unigram_dist**(0.75)))

#构造损失函数
class NegativeSamplingLoss(nn.Module):
    def __init__(self):
        super.__init__()

    def forward(self, input_vectors, output_vectors, noise_vectors):
        batch_size, embed_size = input_vectors.shape

        #input vectors should be a batch of column vectors
        input_vectors = input_vectors.view(batch_size, embed_size, 1)

        # output vectors should be a batch of row vectors
        output_vectors = output_vectors.view(batch_size, 1, embed_size)

        #correct log-sigmoid loss
        out_loss = torch.bmm(output_vectors, input_vectors).sigmoid().log()
        out_loss = out_loss.squeeze()

        # incorrect log-sigmoid loss
        noise_loss = torch.bmm(noise_vectors.neg(), input_vectors).sigmoid().log() #batch size 5
        noise_loss = noise_loss.squeeze().sum(1)

        return -(out_loss + noise_loss).mean()

#模型训练